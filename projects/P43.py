import cv2
import numpy as np
from helpers import cvh_helper as cvh
from helpers import motors as mts
from PIL import Image

# Input source
cap = cv2.VideoCapture(0)


# Loop function
def loop():
    print("Detecting paper")
    mts.forward()
    while True:
        ret, image = cap.read()
        if not ret:
            break

        paper = image
        ret, thresh_gray = cv2.threshold(cv2.cvtColor(paper, cv2.COLOR_BGR2GRAY), 170, 255, cv2.THRESH_BINARY)
        image, contours, _ = cv2.findContours(thresh_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        if len(contours) > 0:
            max_cnt = cvh.get_biggest_contour(contours)
            if cvh.get_img_size2(image) / 2.75 < cv2.contourArea(max_cnt) < cvh.get_img_size2(image) / 1.5:
                _, _, angle = cv2.fitEllipse(max_cnt)
                if 80 < angle < 100:
                    rect = cv2.minAreaRect(max_cnt)
                    box = cv2.boxPoints(rect)
                    box = np.int0(box)
                    print(angle)
                    mts.stop()
                    cv2.drawContours(paper, [box], 0, (0, 255, 0), 1)

        # img2 = Image.fromarray(paper, 'RGB')
        # img2.show()
        cv2.imshow('frame', paper)

        # img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # kernel_size = 5
        # blur_gray = cv2.GaussianBlur(img_gray, (kernel_size, kernel_size), 0)
        # canny = cv2.Canny(blur_gray, 100, 200)
        # _, contours, hierarchy = cv2.findContours(canny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #
        # for i in range(len(contours)):
        #     contour = contours[i]
        #     epsilon = 0.008 * cv2.arcLength(contour, False)
        #     approx = cv2.approxPolyDP(contour, epsilon, True)
        #     if len(approx) == 4 and cv2.contourArea(contour) > 10:
        #         cv2.drawContours(image, contours, i, (0, 255, 0), 1)
        #         cv2.putText(image, str("RECTANGLE"), tuple(approx[0][0]),
        #                 cv2.FONT_HERSHEY_SIMPLEX, 1.0, [255, 255, 0], 2, cv2.LINE_AA)



        show_contours = []

        # parent = None
        # for i in range(len(contours)):
        #     contour = contours[i]
        #     if cv2.arcLength(contour, True) \
        #             and cv2.contourArea(contour) > 20000:
        #         if hierarchy[0][i][2] != -1:
        #             epsilon = 0.008 * cv2.arcLength(contour, True)
        #             approx = cv2.approxPolyDP(contour, epsilon, True)
        #             if len(approx) == 4:
        #                 parent = i
        #             if len(approx) == 7:
        #                 if hierarchy[0][i][3] == parent:
        #                     arrow_pointing = get_direction(contours[i], image, approx)
        #                     show_contours.append(contours[i])
        #
        # cv2.drawContours(image, show_contours, -1, (0, 255, 0), 1)
        # cv2.imshow('frame', image)


        # Exit loop with 'q'
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    capture.release()
    cv2.destroyAllWindows()


# Function calls
loop()
