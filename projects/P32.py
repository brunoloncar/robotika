import cv2
from helpers import cvh_helper as cvh
from helpers import motors as mts
import time

# Constants
green_lower, green_upper = 44, 65
yellow_lower, yellow_upper = 25, 35
blue_lower, blue_upper = 75, 130
orange_lower, orange_upper = 5, 25


# Input source
capture = cv2.VideoCapture(0)


# Loop function
def loop():

    print("Detecting colors")

    while True:

        # Load and convert video input image
        ret, img_bgr, img_hsv = cvh.read_and_convert_video(capture)
        if not ret:
            break

        # Get covers for colors
        cover_green = contour_covers_view(img_hsv, green_lower, green_upper)
        cover_yellow = contour_covers_view(img_hsv, yellow_lower, yellow_upper)
        cover_blue = contour_covers_view(img_hsv, blue_lower, blue_upper, 0.15)
        cover_orange = contour_covers_view(img_hsv, orange_lower, orange_upper, 0.15)

        if cover_green or cover_yellow or cover_blue or cover_orange:
            process_cover(cover_green, cover_yellow, cover_blue, cover_orange)

        cv2.imshow('Light green object detection', img_bgr)

        # Exit loop with 'q'
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    capture.release()
    cv2.destroyAllWindows()


def contour_covers_view(image, lower, upper, view_percent=0.5):
    mask = cvh.get_mask(image, lower, upper)
    contours = cvh.get_contours(mask)
    if len(contours) > 0:
        # Get max contour area
        max_cnt = cvh.get_biggest_contour(contours)
        if cv2.contourArea(max_cnt) > cvh.get_img_size(image) * view_percent:
            return True
        return False


def process_cover(cover_green, cover_yellow, cover_blue, cover_orange):
    if cover_green:
        print("Green logic")
        mts.forwardThenStop(3)
    elif cover_yellow:
        print("Yellow logic")
        mts.backwardThenStop(3)
    elif cover_blue and cover_orange:
        print("Orange logic")
        mts.rotateOneCircle()


# Function calls
loop()
