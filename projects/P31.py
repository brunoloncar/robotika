import cv2
from helpers import cvh_helper as cvh
from helpers import motors as mts
import time

# Constants
green_lower, green_upper = 44, 65

# Input source
capture = cv2.VideoCapture(0)


# Loop function
def loop():
    # Rotate
    mts.rotateRight()
    print("Detecting colors")

    while True:

        # Load and convert video input image
        ret, img_bgr, img_hsv = cvh.read_and_convert_video(capture)
        if not ret:
            break

        # Get mask and contours
        mask_green = cvh.get_mask(img_hsv, green_lower, green_upper)
        contours = cvh.get_contours(mask_green)

        if len(contours) > 0:
            # Get max contour area
            max_cnt = cvh.get_biggest_contour(contours)
            # Stop rotation if area bigger than 50%
            if cv2.contourArea(max_cnt) > cvh.get_img_size(img_bgr) / 2:
                print("Saw the light green object...")
                mts.forwardBit()

        cv2.imshow('Light green object detection', img_bgr)

        # Exit loop with 'q'
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    capture.release()
    cv2.destroyAllWindows()


def rotate_robot():
    print("I'm rotating.")


# Function calls
loop()
