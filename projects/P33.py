import cv2
from helpers import cvh_helper as cvh
from helpers import motors as mts
import time

# Constants
red_lower1, red_upper1 = 0, 15
red_lower2, red_upper2 = 160, 180
yellow_lower, yellow_upper = 16, 30
green_lower, green_upper = 45, 75


# Loop function
def loop():
    print("Detecting traffic light")

    while True:
        # Input source
        capture = cv2.VideoCapture(0)

        ret, img_bgr, img_hsv = cvh.read_and_convert_video(capture)
        if not ret:
            break

        ellipse_red, ellipse_yellow, ellipse_green = get_ellipses(img_hsv, img_bgr)

        if ellipse_red or ellipse_yellow or ellipse_green:
            process_ellipses(ellipse_red, ellipse_yellow, ellipse_green)

        cv2.imshow('Traffic light detection', img_bgr)

        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break
        
        capture.release()
    
    cv2.destroyAllWindows()


# Functions
def get_ellipses(img_hsv, img_bgr):
    # Red region
    mask_red_1 = cvh.get_mask(img_hsv, red_lower1, red_upper1, min_sat=110, min_val=60)
    mask_red_2 = cvh.get_mask(img_hsv, red_lower2, red_upper2, min_sat=110, min_val=60)
    mask_red = mask_red_1 | mask_red_2

    ellipse_red = cvh.create_ellipse(mask_red, img_bgr)

    # Yellow region
    mask_yellow = cvh.get_mask(img_hsv, yellow_lower, yellow_upper, min_sat=110, min_val=60)
    ellipse_yellow = cvh.create_ellipse(mask_yellow, img_bgr)

    # Green region
    mask_green = cvh.get_mask(img_hsv, green_lower, green_upper)
    ellipse_green = cvh.create_ellipse(mask_green, img_bgr)

    return ellipse_red, ellipse_yellow, ellipse_green


def process_ellipses(ellipse_red, ellipse_yellow, ellipse_green):
    if ellipse_red and not ellipse_yellow and not ellipse_green:
        print("I'm continuing stopping.")
        mts.stop()
    elif ellipse_red and ellipse_yellow and not ellipse_green:
        print("Red and yellow.. GO GO GO..")
        mts.stopThenForward()
        time.sleep(2)
    elif not ellipse_red and ellipse_yellow and not ellipse_green:
        print("It is going to be red now so I am stopping.")
        mts.stop()
    elif not ellipse_red and not ellipse_yellow and ellipse_green:
        print("It's green so I'm driving a bit.")
        mts.forwardThenStop(5)
        time.sleep(2)
    else:
        print("[{}] I see: Red ({}), Yellow ({}), Green ({})".format(
            cvh.get_timestamp(), ellipse_red, ellipse_yellow, ellipse_green))
        time.sleep(1.5)



# Function calls
loop()
