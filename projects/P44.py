import cv2
from helpers import cvh_helper as cvh
from helpers import motors as mts
import time
import math


def get_direction(contour, image, approx):
    leftmost = tuple(contour[contour[:, :, 0].argmin()][0])
    rightmost = tuple(contour[contour[:, :, 0].argmax()][0])
    topmost = tuple(contour[contour[:, :, 1].argmin()][0])
    bottommost = tuple(contour[contour[:, :, 1].argmax()][0])

    moment = cv2.moments(contour)
    cx = int(moment['m10'] / moment['m00'])
    cy = int(moment['m01'] / moment['m00'])

    distance_top_bot = math.sqrt(((topmost[0] - bottommost[0]) ** 2) + ((topmost[1] - bottommost[1]) ** 2))
    distance_left_right = math.sqrt(((leftmost[0] - rightmost[0]) ** 2) + ((leftmost[1] - rightmost[1]) ** 2))

    cv2.circle(image, (cx, cy), 3, (0, 255, 255), -1)
    cv2.circle(image, rightmost, 3, (0, 0, 255), -1)
    cv2.circle(image, leftmost, 3, (0, 0, 255), -1)
    cv2.circle(image, topmost, 3, (0, 0, 255), -1)
    cv2.circle(image, bottommost, 3, (0, 0, 255), -1)

    x,y,w,h = cv2.boundingRect(contour)
    aspect = float(w)/float(h)

    # if float(1.8) < aspect < float(2.0):
    if topmost[0] > cx and bottommost[0] > cx:
        cv2.putText(image, "RIGHT", tuple(approx[0][0]),
                    cv2.FONT_HERSHEY_SIMPLEX, 1.0, [255, 255, 0], 2, cv2.LINE_AA)
        return "right"
    elif topmost[0] < cx and bottommost[0] < cx:
        cv2.putText(image, "LEFT", tuple(approx[0][0]),
                    cv2.FONT_HERSHEY_SIMPLEX, 1.0, [255, 255, 0], 2, cv2.LINE_AA)
        return "left"

    return None


# Loop function
def loop():
    
    print("Detecting arrow")
    mts.forward()
    while True:
        
        # Input source
        capture = cv2.VideoCapture(0)
        
        ret, image, img_hsv = cvh.read_and_convert_video(capture)
        if not ret:
            break

        img_blur = cv2.GaussianBlur(image, (3, 3), 0)
        edges = cv2.Canny(img_blur,40,200)
        img_gray = cv2.cvtColor(img_blur, cv2.COLOR_BGR2GRAY)
        _, img_bin = cv2.threshold(edges, 110, 255, cv2.THRESH_BINARY)
        _, contours, hierarchy = cv2.findContours(img_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        show_contours = []

        parent = None
        for i in range(len(contours)):
            contour = contours[i]
            if cv2.arcLength(contour, True) \
                    and cv2.contourArea(contour) > 2000:
                if hierarchy[0][i][2] != -1:
                    epsilon = 0.008 * cv2.arcLength(contour, True)
                    approx = cv2.approxPolyDP(contour, epsilon, True)
                    if len(approx) == 7:
                        arrow_pointing = get_direction(contours[i], image, approx)
                        print(arrow_pointing)

                        if arrow_pointing == "left":
                            mts.turnLeftThenForward()
                            time.sleep(1.5)
                        elif arrow_pointing == "right":
                            mts.turnRightThenForward()
                            time.sleep(1.5)
                        cv2.drawContours(image, contours, i, (0, 255, 0), 1)
                        cv2.putText(image, str(len(approx)), tuple(approx[0][0]),
                                    cv2.FONT_HERSHEY_SIMPLEX, 1.0, [255, 255, 0], 2, cv2.LINE_AA)

        # cv2.drawContours(image, contours, -1, (0, 255, 0), 1)
        cv2.imshow('frame', img_bin)


        # Exit loop with 'q'
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

        capture.release()

    cv2.destroyAllWindows()


# Function calls
loop()
