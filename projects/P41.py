import cv2
from helpers import cvh_helper as cvh
from helpers import motors as mts
from time import sleep

# Input source
capture = cv2.VideoCapture(0)

# Loop function
def loop():
    print("Calculating FPS...")
    cvh.get_write_fps(capture)
    print("Detecting smile...")
    
    while True:
        ret, image = capture.read()
        if not ret:
            break

        if cvh.detectSmile(image):
            print("Found a smile - :D")
            mts.rotateOneCircle()

        cv2.imshow('frame', image)

        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break
    
    capture.release()
    cv2.destroyAllWindows()

# Function calls
loop()
